import express from 'express';
import cors from 'cors';
import { MongoClient } from 'mongodb';
import dotenv from 'dotenv';
// Configure dotenv
dotenv.config();

// const mongoURI = 'mongodb://localhost:27017'; // Default MongoDB connection URL
const mongoURI ='mongodb+srv://amankumar:TestPassword@test.kjjcjuv.mongodb.net/users?retryWrites=true&w=majority&appName=test';
// Database Name
const dbName="users"; // Change this to your database name

// Create a new MongoClient
const client = new MongoClient(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });
const app=express();
app.use(cors());
const PORT = parseInt(process.env.PORT) || 8000;
app.get('/users', async (req, res) => {
    try {
        // Connect to the MongoDB server
      await client.connect((err) => {
        if (err) {
            console.error('Failed to connect to MongoDB:', err);
            return;
        }
        console.log('Connected successfully to MongoDB');
    });
      const db = client.db(dbName);
       // Perform database operations here
      const collection = db.collection('users');
      const data = await collection.find().toArray();
      res.json(data);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Server error' });
    } finally {
        // Close the connection when done
      await client.close();
    }
  });
  
app.get('/', (req, res) => {
    res.send('Hello, Express and AMAN!');
})
app.get('/data', (req, res) => {
    let arr=[
        {"id":101,"name": "Aman Kumar"},
        {"id":102,"name": "Pupu"}
    ];
    res.send(arr);
})
app.listen(PORT,()=>{
    console.log("Server is running on port:",PORT);
});